##Creación de un nuevo proyecto con Composer y PHPUnit

¡Hola! Bienvenido al nuevo curso Crea Componentes para PHP y Laravel.

Sé que estás impaciente en aprender cómo crear componentes, subirlos a packagist, obtener miles de descargas y estrellas en GitHub y hacerte famoso, nosotros también, pero antes de comenzar, hay muchos conceptos que necesitarás aprender, puesto que no sólo son necesarios para crear un buen componente, sino que también te ayudarán muchísimo a entender mejor Laravel y otros frameworks o herramientas con las que desarrollas a diario.

Quizás aún arrugas la cara cuando oyes la palabra “Facade” o los términos “inyección de dependencias” o “pruebas unitarias” te producen dolor de cabeza. No te preocupes, después de terminado este curso esos y muchos otros paradigmas avanzados te serán familiares y tu nivel como desarrollador aumentará muchísimo.


#####Conocimientos previos
En esta lección aprenderemos cómo usar Composer para manejar las dependencias de PHP necesarias en el desarrollo de un componente. Si tienes dudas o aún no tienes instalado Composer, consulta el tutorial sobre qué es y su uso:

* [¿Qué es Composer? (tutorial).](https://styde.net/que-es-composer-y-como-usarlo/).
* [Instalación y uso de Composer (videotutorial).](https://styde.net/aprende-laravel-5-instalacion-y-uso-de-composer/).
* [Ver todos nuestros tutoriales de Composer](https://styde.net/composer-manejador-paquetes-php/).

Además debes conocer sobre la especificación PSR-4, la cual le permite a Composer cargar las clases automáticamente.  De ser necesario, puedes revisar qué es PSR-4 y el uso de namespaces (videotutorial).

Adicionalmente, requerirás instalar dependencias a través de Composer, puedes repasar sobre cómo instalar y actualizar paquetes con Composer.

En esta lección vamos a implementar pruebas unitarias mediante PHPUnit, las cuales serán las encargadas de probar los métodos o funciones de una clase como una «unidad».  Puedes repasar sobre el diseño de clases con pruebas unitarias en PHPUnit en nuestro curso introductorio de Laravel 5.1.

Si bien no indispensable, ayuda muchísimo aprender qué son y cómo utilizar pruebas de integración ANTES de entrar en el mundo de pruebas unitarias, así que si tienes algo de tiempo extra, asegúrate de ver la parte 3 de nuestro curso introductorio de Laravel 5.1 donde cubrimos de manera extensa el nuevo componente de pruebas de integración.

####Notas:
Para está lección, se usó el siguiente comando para instalar PHPUnit:

```php
composer require --dev phpunit/phpunit
```

Será --dev pues PHPUnit sólo es necesario para el desarrollo del componente.

Luego de instalar PHPUnit y crear la primera prueba en el directorio tests/ deberás ejecutarla en consola así:

```php
vendor/bin/phpunit tests/ --colors
```

Sin embargo, después de creado el archivo de configuración phpunit.xml por defecto, sólo basta ejecutar en la consola:

```php
vendor/bin/phpunit
```

Si deseas crear un alias puedes hacerlo también desde la consola:

```php
alias t=vendor/bin/phpunit
```

####Actividades:
* Investiga sobre los otros métodos de prueba (con el prefijo «assert») que nos provee PHPUnit
* Busca información de otras configuraciones que se pueden colocar en el archivo phpunit.xml y añádelas a tu archivo de configuración.