## Dependencias y código acoplado

Cuando trabajamos en programación orientada a objetos es normal encontrarse con clases y objetos que interactuan y dependen unos de otros, la idea de ello es aprovechar al máximo las virtudes de cada uno y evitar reescribir código innecesario.

Sin embargo esto nos puede llevar a código que esté innecesariamente «acoplado», es decir objetos y clases que no puedan funcionar sin la presencia de otros objetos y clases.

Esto trae varios problemas, por un lado el código es menos reusable y por otro lado es más difícil de probar y depurar.

En la lección de hoy veremos un ejemplo de esto, mientras avanzamos en la creación de algunas clases base que nos permitirán más adelante comparar código y aprender sobre la inyección de dependencias y otros conceptos más avanzados.

#### Repositorio

Mira el código en GitHub: [Actual](https://github.com/StydeNet/unit-tests/tree/1.1), [resultado](https://github.com/StydeNet/unit-tests/tree/1.2), [comparación](https://github.com/StydeNet/unit-tests/compare/1.1...1.2).