## Creación y uso de Interfaces y Stubs

Cuando trabajamos con la programación orientada a objetos hay términos que debemos entender y uno de ellos es el polimorfismo, el cual es la capacidad de que un mismo método pueda tener distintos comportamientos dependiendo del contexto en el que se está ejecutando.  Esto en PHP se puede lograr por ejemplo con el uso de interfaces.

En esta lección 4 del curso Crea componentes para PHP y Laravel aprenderás a crear e implementar interfaces así como stubs que te ayudarán a hacer el código más reusable y desacoplado,  además de que facilitará la implementación de las pruebas unitarias.

Esto nos permitirá ya no trabajar con un archivo para manejar las sesiones, sino que usaremos un nuevo manejador (driver) con el cual podemos cargar datos de sesión con un simple array. Esto es un ejemplo muy sencillo de cómo funciona el manejador de Sesiones en frameworks como Symfony o Laravel.


### Conocimientos previos
Una Interfaz establece qué debe hacer la clase que la implementa, sin especificar el cómo, es decir que actúa como una plantilla. Puedes revisar Aprende qué son las interfaces, cómo usarlas y definirlas en PHP y Laravel si quieres ahondar más en el tema.

Por otro lado, un Stub es un tipo de clases que emula el comportamiento de otra clase que podemos emplear para avanzar en la construcción de nuestro sistema y no tener que esperar que esa clase esté implementada. Para conocer un poco más sobre interfaces junto con stubs en PHP pueden consultar Sustituye implementaciones usando Stubs e Interfaces en PHP (POO)

#### Notas
Intenta siempre leer y analizar los errores que ocurren mientras programas, pues de ellos también se aprende
Ten en cuenta que no debes implementar todos los patrones que aprendas en el 100% de los casos, sino sólo cuando es necesario.