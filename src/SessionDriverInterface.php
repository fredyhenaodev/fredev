<?php

namespace Fredev;

interface SessionDriverInterface
{
    public function load();
}