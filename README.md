## Curso de creación de componentes para PHP y Laravel

Este curso está diseñado para enseñarte a escribir código profesional en PHP de tal manera que puedas crear tus propios componentes y paquetes para ser utilizados en diferentes proyectos en conjunto o no con el framework Laravel.

En este curso te enseñaremos desde conceptos claves que te permitirán dominar la programación orientada a objetos y entender cómo funciona Laravel (por ejemplo: qué es la inyección de dependencias, qué es un facade, etc.), hasta la creación de componentes y su publicación en GitHub y Packagist. Además aprenderás de forma extensa a escribir pruebas unitarias con PHPUnit.

En las siguientes lecciones aprenderás nuevas metodologías de desarrollo aplicable a cada uno de tus proyectos. A lo largo de esta serie verás paso a paso el proceso llevado a cabo para crear un componente completamente funcional, utilizando inyección de dependencias, pruebas de integración, manejo de Composer, PHPUnit y otros temas de nivel avanzado.